/** 
 * @file exofloaterprogress.h
 * @brief EXOFloaterProgress class definition
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_PROGRESS
#define EXO_FLOATER_PROGRESS

#include "llfloater.h"
#include "llevents.h"

class LLButton;
class LLProgressBar;

class exoFloaterProgress : public LLFloater
{
	friend class LLFloaterReg;

private:
	exoFloaterProgress(const LLSD& key);
	~exoFloaterProgress();
	
	static exoFloaterProgress* sInstance;

	BOOL postBuild();
	
public:
	void setText(const std::string& text);
	void setPercent(const F32 percent);

	void setCancelButtonVisible(BOOL b, const std::string& label);

	bool handleUpdate(const LLSD& event_data);
	
	static exoFloaterProgress* getInstance(); // Dunno, I think this helps. :D

private:
	static void onCancelButtonClicked( void* );

	LLProgressBar* mProgressBar;
	F32 mPercentDone;
	LLButton*	mCancelBtn;
};

#endif // EXO_FLOATER_PROGRESS

/** 
 * @file exopanelpreferences.cpp
 * @brief Side tray Exodus settings panel
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"
#include "exopanelpreferences.h"
#include "lltabcontainer.h"

#include "exosystems.h"

#include "llfloaterreg.h"
#include "llkeyboard.h"
#include "llmodaldialog.h"
#include "llviewercontrol.h"
#include "llspinctrl.h"
#include "llsliderctrl.h"
#include "llcheckboxctrl.h"

// exo_floater_sidetray.xml

class exoRequestTeleportSetKeyDialog : public LLModalDialog
{
public:
	exoRequestTeleportSetKeyDialog(const LLSD& key);
	~exoRequestTeleportSetKeyDialog(){};
	
	BOOL postBuild();

	void setStuff(exoFloaterPreferences* parent, bool type){ mParent = parent; mType = type; }

	BOOL handleKeyHere(KEY key, MASK mask);
	static void onCancel(void* user_data);
		
private:
	exoFloaterPreferences* mParent;
	bool mType;
};

exoRequestTeleportSetKeyDialog::exoRequestTeleportSetKeyDialog(const LLSD& key) : LLModalDialog(key), mParent(NULL)
{
}

BOOL exoRequestTeleportSetKeyDialog::postBuild()
{
	childSetAction("Cancel", onCancel, this);
	getChild<LLUICtrl>("Cancel")->setFocus(TRUE);
	
	gFocusMgr.setKeystrokesOnly(TRUE);

	return TRUE;
}

BOOL exoRequestTeleportSetKeyDialog::handleKeyHere(KEY key, MASK mask)
{
	BOOL result = TRUE;

	if ((key == 'Q' && mask == MASK_CONTROL) || key == KEY_ESCAPE ||
		key == 'W' || key == 'A' || key == 'S' || key == 'D' || key == 'Q' || key == 'Z' ||
		key == KEY_UP || key == KEY_LEFT || key == KEY_DOWN || key == KEY_RIGHT ||
		key == 'E' || key == KEY_PAGE_UP)
	{
		result = FALSE;
	}
	else if (mParent) mParent->setKey(key, mType);

	if (result) closeFloater();

	return result;
}

void exoRequestTeleportSetKeyDialog::onCancel(void* user_data)
{
	exoRequestTeleportSetKeyDialog* self = (exoRequestTeleportSetKeyDialog*)user_data;
	self->closeFloater();
}

exoFloaterPreferences::exoFloaterPreferences(const LLSD& key) : LLFloater(key)
{
	LLFloaterReg::add("request_teleport_set_key", "exo_request_teleport_key.xml", (LLFloaterBuildFunc)&LLFloaterReg::build<exoRequestTeleportSetKeyDialog>);

	mCommitCallbackRegistrar.add("Exodus.SetKey", boost::bind(&exoFloaterPreferences::onClickSetKey, this, _2));
	mCommitCallbackRegistrar.add("Exodus.PreviewSound", boost::bind(&exoFloaterPreferences::onClickPreviewSound, this, _2));
	mCommitCallbackRegistrar.add("Exodus.SetScriptTheme", boost::bind(&exoFloaterPreferences::onClickScriptTheme, this, _2));
	mCommitCallbackRegistrar.add("Exodus.SetSkin", boost::bind(&exoFloaterPreferences::onClickChangeSkin, this, _2));
}

BOOL exoFloaterPreferences::postBuild()
{
	mScriptThresholdCheck = getChild<LLCheckBoxCtrl>("alert_script_threshold");
	mScriptThresholdCheck->setCommitCallback(boost::bind(&exoFloaterPreferences::toggleScriptThreshold, this));
	
	LLSliderCtrl* slider = getChild<LLSliderCtrl>("alert_script_threshold_1");
	LLSpinCtrl* spinner  = getChild<LLSpinCtrl>("alert_script_threshold_2");
	
	if (gSavedSettings.getS32("ExodusAlertScriptThreshold"))
	{
		slider->setEnabled(TRUE);
		spinner->setEnabled(TRUE);
	}
	else
	{
		slider->setEnabled(FALSE);
		spinner->setEnabled(FALSE);
	}

	return TRUE;
}

void exoFloaterPreferences::onOpen(const LLSD& key)
{
}

bool exoFloaterPreferences::notifyChildren(const LLSD& info)
{
	return FALSE;
}

void exoFloaterPreferences::toggleScriptThreshold()
{
	LLSliderCtrl* slider = getChild<LLSliderCtrl>("alert_script_threshold_1");
	LLSpinCtrl* spinner  = getChild<LLSpinCtrl>("alert_script_threshold_2");

	if (mScriptThresholdCheck->getValue().asBoolean())
	{
		gSavedSettings.setS32("ExodusAlertScriptThreshold", scriptThreshold);

		slider->setEnabled(TRUE);
		spinner->setEnabled(TRUE);
	}
	else
	{
		scriptThreshold = gSavedSettings.getS32("ExodusAlertScriptThreshold");
		gSavedSettings.setS32("ExodusAlertScriptThreshold", 0);

		slider->setEnabled(FALSE);
		spinner->setEnabled(FALSE);
	}
}

void exoFloaterPreferences::onClickPreviewSound(const LLSD& user_data)
{
	make_ui_sound(user_data.asString().c_str());
}

void exoFloaterPreferences::onClickSetKey(const LLSD& user_data)
{
	exoRequestTeleportSetKeyDialog* dialog = LLFloaterReg::showTypedInstance<exoRequestTeleportSetKeyDialog>("request_teleport_set_key", LLSD(), TRUE);
	if (dialog)
	{
		dialog->setStuff(this, user_data.asString() == "Accept");
		dialog->center();
	}
}

void exoFloaterPreferences::setKey(KEY key, bool type) // Todo: Replace the bool with the childname?
{
	if (type)
	{
		getChild<LLUICtrl>("request_teleport_accept_key")->setValue(LLKeyboard::stringFromKey(key));
		getChild<LLUICtrl>("request_teleport_accept_key")->onCommit();
	}
	else
	{
		getChild<LLUICtrl>("request_teleport_decline_key")->setValue(LLKeyboard::stringFromKey(key));
		getChild<LLUICtrl>("request_teleport_decline_key")->onCommit();
	}
}

void exoFloaterPreferences::onClickScriptTheme(const LLSD& user_data)
{
	if (user_data.asString() == "Bright")
	{
		gSavedSettings.setBOOL("ExodusScriptEditorCustomized", FALSE);
		gSavedSettings.setString("ExodusScriptKeywordsFile", "keywords_bright.ini");
		gSavedSettings.setColor4("ExodusScriptFunctionColor", LLColor4(0.498f, 0.f, 0.149f, 1.f));
	}
	else
	{
		gSavedSettings.setBOOL("ExodusScriptEditorCustomized", TRUE);
		gSavedSettings.setString("ExodusScriptKeywordsFile", "keywords_dark.ini");
		gSavedSettings.setColor4("ExodusScriptFunctionColor", LLColor4(0.9f, 0.137f, 0.137f, 1.f));
	}
}

void exoFloaterPreferences::onClickChangeSkin(const LLSD& user_data)
{
	if (user_data.asString() == "vanilla")
	{
		gSavedSettings.setString("SkinCurrent", "vanilla");
	}
	else
	{
		gSavedSettings.setString("SkinCurrent", "default");
	}
}

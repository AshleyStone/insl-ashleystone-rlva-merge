/** 
 * @file exofloaterteleportrequest.cpp
 * @brief The request teleport accept/decline window.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "llfloaterreg.h"

#include "exofloaterteleportrequest.h"

#include "llagent.h"
#include "llagentcamera.h"
#include "llagentui.h"
#include "llbutton.h"
#include "llpanelblockedlist.h"
#include "llprogressbar.h"
#include "llmutelist.h"
#include "lltimer.h"
#include "llviewercontrol.h"
#include "llviewermessage.h"

exoFloaterTeleportRequest::exoFloaterTeleportRequest(const LLSD& key) :
	LLFloater(key)
{
	closeTimer.stop();

	setAutoFocus(FALSE); // Let's not steal focus. :(
	setVisible(FALSE); // Let's not be visible till we have data?
}

BOOL exoFloaterTeleportRequest::postBuild()
{
	if (gSavedSettings.getBOOL("ExodusTeleportRequestPlaySoundAlert"))
	{
		make_ui_sound("ExodusTeleportRequestSoundAlert");
	}

	mProgressBar = getChild<LLProgressBar>("timeout_progress_bar");
	
	getChild<LLButton>("accept_btn")->setClickedCallback(boost::bind(&exoFloaterTeleportRequest::onAcceptButtonClicked, this));
	getChild<LLButton>("decline_btn")->setClickedCallback(boost::bind(&exoFloaterTeleportRequest::onDeclineButtonClicked, this));
	getChild<LLButton>("mute_btn")->setClickedCallback(boost::bind(&exoFloaterTeleportRequest::onMuteButtonClicked, this));

	return TRUE;
}

void exoFloaterTeleportRequest::setData(std::string from_name, LLUUID from_id, std::string message)
{
	targetName = from_name;
	targetKey = from_id;
	targetMessage = message;

	setTitle(targetName + " has requested a teleport!");

	closeTimer.start();

	fixPosition();

	setVisible(TRUE);
}

void exoFloaterTeleportRequest::draw()
{
	if (previousCamera != gAgentCamera.cameraMouselook()) fixPosition();

	if (closeTimer.getStarted())
	{
		F32 progress = closeTimer.getElapsedTimeF32();
		if (progress > 15.f)
		{
			if (gSavedSettings.getBOOL("ExodusTeleportRequestPlayDeclineSoundAlert"))
			{
				make_ui_sound("ExodusTeleportRequestDeclineSoundAlert");
			}

			requestReply("timeout");
			closeFloater();
			destroy();

			return;
		}

		mProgressBar->setValue(LLSD(progress * 6.666f).asReal());
	}

	LLFloater::draw();
}

void exoFloaterTeleportRequest::fixPosition()
{
	previousCamera = gAgentCamera.cameraMouselook();

	if (previousCamera)
	{
		LLRect bounds = LLRect(gFloaterView->getRect());

		S32 left   = bounds.mLeft + (bounds.getWidth() - getRect().getWidth()) / 2;
		S32 bottom = bounds.mBottom + (bounds.getHeight() - getRect().getHeight()) / 12;

		setOrigin(left, bottom);
	}
	else center();
}

void exoFloaterTeleportRequest::requestReply(std::string suffix)
{
	if (!(targetMessage.empty() || targetKey.isNull()))
	{
		if (suffix.empty())
		{
			LLMessageSystem* msg = gMessageSystem;

			msg->newMessageFast(_PREHASH_StartLure);
			msg->nextBlockFast(_PREHASH_AgentData);
			msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
			msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
			msg->nextBlockFast(_PREHASH_Info);
			msg->addU8Fast(_PREHASH_LureType, (U8)0); 
			msg->addStringFast(_PREHASH_Message, targetMessage);
			msg->nextBlockFast(_PREHASH_TargetData);
			msg->addUUIDFast(_PREHASH_TargetID, targetKey);

			gAgent.sendReliableMessage();
		}
		else
		{
			std::string my_name;
			LLAgentUI::buildFullname(my_name);

			pack_instant_message(
				gMessageSystem,
				gAgent.getID(), FALSE,
				gAgent.getSessionID(),
				targetKey, my_name,
				targetMessage + "|" + suffix,
				IM_ONLINE, IM_TYPING_STOP
			);

			gAgent.sendReliableMessage();
		}
	}

	closeTimer.stop();
	closeFloater();
	destroy();
}

void exoFloaterTeleportRequest::onAcceptButtonClicked()
{
	if (gSavedSettings.getBOOL("ExodusTeleportRequestPlayAcceptSoundAlert"))
	{
		make_ui_sound("ExodusTeleportRequestAcceptSoundAlert");
	}

	requestReply();
}

void exoFloaterTeleportRequest::onDeclineButtonClicked()
{
	if (gSavedSettings.getBOOL("ExodusTeleportRequestPlayDeclineSoundAlert"))
	{
		make_ui_sound("ExodusTeleportRequestDeclineSoundAlert");
	}

	requestReply("decline");
}

void exoFloaterTeleportRequest::onMuteButtonClicked()
{
	LLMuteList::getInstance()->add(LLMute(targetKey, targetName, LLMute::AGENT));
	LLPanelBlockedList::showPanelAndSelect(targetKey);

	if (gSavedSettings.getBOOL("ExodusTeleportRequestPlayDeclineSoundAlert"))
	{
		make_ui_sound("ExodusTeleportRequestDeclineSoundAlert");
	}

	requestReply("decline");
}

/** 
 * @file exosystems.cpp
 * @brief System used for downloading client detection database and other bits.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exochatcommands.h"

#include "exobridge.h"

#include "llagent.h"
#include "llagentcamera.h"
#include "llcalc.h"
#include "llcommandhandler.h"
#include "llchathistory.h"
#include "llnearbychat.h"
#include "llnotificationhandler.h"
#include "llnotificationmanager.h"
#include "llnotificationsutil.h"
#include "llviewercontrol.h"
#include "llviewerregion.h"
#include "llvoavatarself.h"
#include "llvolume.h"
#include "llvolumemessage.h"
#include "llworld.h"
#include "llviewerobjectlist.h"

#include "aoengine.h"

#include "material_codes.h"

bool exoChatCommands::phraseChat(std::string data)
{
	if (data.empty()) return false;

	if (eAPI::phraseChat(data, NULL, gAgent.getID()))return TRUE;


	LLStringUtil::toLower(data);

	std::string command;
	std::istringstream get(data);
	if (!(get >> command)) return false;
	
	const std::string INVALID_CHAT_COMMAND = "Bad chat command input detected!";
	
	static LLCachedControl<std::string> sAOToggleCommand(gSavedSettings,		"ExodusChatCommandAOToggle",			"/cao");
	static LLCachedControl<std::string> sCalculateCommand(gSavedSettings,		"ExodusChatCommandCalculate",			"/calc");
	static LLCachedControl<std::string> sClearLocalChatCommand(gSavedSettings,	"ExodusChatCommandClearLocalChat",		"/clr");
	static LLCachedControl<std::string> sDrawDistanceCommand(gSavedSettings,	"ExodusChatCommandDrawDistance",		"/dd");
	static LLCachedControl<std::string> sMapToCommand(gSavedSettings,			"ExodusChatCommandMapTo",				"/mapto");
	static LLCachedControl<std::string> sRezPlatformCommand(gSavedSettings,		"ExodusChatCommandRezPlatform",			"/rezplat");
	static LLCachedControl<std::string> sSetHomeCommand(gSavedSettings,			"ExodusChatCommandSetHome",				"/sethome");
	static LLCachedControl<std::string> sTeleportHomeCommand(gSavedSettings,	"ExodusChatCommandTeleportHome",		"/home");
	static LLCachedControl<std::string> sTeleportToGround(gSavedSettings,		"ExodusChatCommandTeleportToGround",	"/flr");
	static LLCachedControl<std::string> sTeleportToHeight(gSavedSettings,		"ExodusChatCommandTeleportToHeight",	"/gth");
	static LLCachedControl<std::string> sTeleportToPosition(gSavedSettings,		"ExodusChatCommandTeleportToPosition",	"/gtp");
	
	if (command == std::string(sAOToggleCommand)) // "/cao"
	{
		std::string status;
		if (get >> status)
		{
			if (status == "on") gSavedPerAccountSettings.setBOOL("ExodusAOEnable", TRUE);
			else if (status == "off") gSavedPerAccountSettings.setBOOL("ExodusAOEnable", FALSE);
			else if (status == "sit")
			{
				AOSet* tmp;
				tmp = AOEngine::instance().getSetByName(AOEngine::instance().getCurrentSetName());
				if (get >> status)
				{
					if(status == "off") AOEngine::instance().setOverrideSits(tmp, TRUE);
					else if(status == "on") AOEngine::instance().setOverrideSits(tmp, FALSE);
				}
				else AOEngine::instance().setOverrideSits(tmp,!tmp->getSitOverride());
			}
			else systemTip(INVALID_CHAT_COMMAND);
		}
		else gSavedPerAccountSettings.setBOOL("ExodusAOEnable", !gSavedPerAccountSettings.getBOOL("ExodusAOEnable"));

		return true;
	}
	else if (command == std::string(sCalculateCommand)) // "/calc"
	{
		if (data.length() > command.length() + 1)
		{
			F32 result = 0.f;

			std::string orig_expression = data.substr(command.length() + 1);
			std::string expression = orig_expression;

			LLStringUtil::toUpper(expression);

			if (LLCalc::getInstance()->evalString(expression, result))
			{
				std::ostringstream output;
				output << "Calculation: " << orig_expression << " = " << result << ".";

				systemMessage(output.str());
			}
			else systemMessage("Calculation \"" + orig_expression + "\" failed.");
		}
		else systemTip(INVALID_CHAT_COMMAND);

		return true;
	}
	else if (command == std::string(sClearLocalChatCommand)) // "/clr"
	{
		LLNearbyChat* local_chat = LLNearbyChat::getInstance();
		if (local_chat && local_chat->mChatHistory) local_chat->mChatHistory->clear();

		return true;
	}
	else if (command == std::string(sDrawDistanceCommand)) // "/dd"
	{
		int draw_distance;
		if (get >> draw_distance)
		{
			gSavedSettings.setF32("RenderFarClip", draw_distance);
			gAgentCamera.mDrawDistance = draw_distance;

			systemTip(llformat("Draw distance has been set to %d.", draw_distance));
		}
		else systemTip(INVALID_CHAT_COMMAND);

		return true;
	}
	else if (command == std::string(sMapToCommand)) // "/mapto"
	{
		int length = command.length() + 1;
		if (data.length() > length)
		{
			LLVector3d pos = gAgent.getPositionGlobal();

			LLSD params;
			params.append(data.substr(length));
			params.append((F32)fmod(pos.mdV[VX], (F64)REGION_WIDTH_METERS));
			params.append((F32)fmod(pos.mdV[VY], (F64)REGION_WIDTH_METERS));
			params.append((F32)pos.mdV[VZ]);

			LLCommandDispatcher::dispatch("teleport", params, LLSD(), NULL, "clicked", true);

			return true;
		}
	}
	else if (command == std::string(sRezPlatformCommand)) // "/rezplat"
	{
		F32 size;
		if (!(get >> size)) size = 10.f;

		LLVector3 myPosition = gAgent.getPositionAgent();
		LLVector3 rezPosition = myPosition;

		F32 myHeight = gAgentAvatarp->getScale().mV[VZ];
		if (myHeight) myHeight /= 2.f;

		rezPosition.mV[VZ] -= myHeight + 0.25f + (gAgent.getVelocity().magVec() * 0.333f);

		LLMessageSystem* msg = gMessageSystem;
		msg->newMessageFast(_PREHASH_ObjectAdd);
		msg->nextBlockFast(_PREHASH_AgentData);
		msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
		msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
		msg->addUUIDFast(_PREHASH_GroupID, gAgent.getGroupID());
		msg->nextBlockFast(_PREHASH_ObjectData);
		msg->addU8Fast(_PREHASH_Material, (U8)3);
		msg->addU32Fast(_PREHASH_AddFlags, myPosition.mV[2] > 4096.0 ? FLAGS_CREATE_SELECTED : (U32)0);

		LLVolumeParams volume_params;
		volume_params.setType(LL_PCODE_PROFILE_SQUARE, LL_PCODE_PATH_LINE);
		volume_params.setBeginAndEndS(0.f, 1.f);
		volume_params.setBeginAndEndT(0.f, 1.f);
		volume_params.setRatio(1, 1);
		volume_params.setShear(0, 0);
		LLVolumeMessage::packVolumeParams(&volume_params, gMessageSystem);

		msg->addU8Fast(_PREHASH_PCode, LL_PCODE_VOLUME);
		msg->addVector3Fast(_PREHASH_Scale, LLVector3(size, size, 0.5f));
		msg->addQuatFast(_PREHASH_Rotation,	LLQuaternion());
		msg->addVector3Fast(_PREHASH_RayStart, rezPosition);
		msg->addVector3Fast(_PREHASH_RayEnd, rezPosition);
		msg->addUUIDFast(_PREHASH_RayTargetID, LLUUID::null);
		msg->addU8Fast(_PREHASH_BypassRaycast, (U8)TRUE);
		msg->addU8Fast(_PREHASH_RayEndIsIntersection, (U8)FALSE);
		msg->addU8Fast(_PREHASH_State, (U8)FALSE);
		msg->sendReliable(gAgent.getRegionHost());

		return true;
	}
	else if (command == std::string(sSetHomeCommand)) // "/sethome"
	{
		gAgent.setStartPosition(START_LOCATION_ID_HOME);
		
		return true;
	}
	else if (command == std::string(sTeleportHomeCommand)) // "/home"
	{
		gAgent.teleportHome();
		
		return true;
	}
	else if (command == std::string(sTeleportToGround)) // "/flr"
	{
		LLVector3d target_position = gAgent.getPositionGlobal();
		target_position.mdV[VZ] = 0.f;

		gAgent.teleportViaLocation(target_position);

		return true;
	}
	else if (command == std::string(sTeleportToHeight)) // "/gth"
	{
		if (get >> command)
		{
			std::istringstream get(command); // Special Thanks: Staypuft Marshmallow Man.

			int z_offset;
			if (!(get >> z_offset)){ systemTip(INVALID_CHAT_COMMAND); return true; }

			command = command.substr(0, 1);
			LLVector3d target_position = gAgent.getPositionGlobal();
			if (command == "+" || command == "-") target_position.mdV[VZ] += z_offset;
			else target_position.mdV[VZ]  = z_offset;

			gAgent.teleportViaLocation(target_position);

			return true;
		}

		systemTip(INVALID_CHAT_COMMAND);

		return true;
	}
	else if (command == std::string(sTeleportToPosition)) // "/gtp"
	{
		F32 x, y, z;
		if (get >> x)  if (get >> y)
		{
			LLVector3d target_position = gAgent.getPositionGlobal();

			if (get >> z) LLVector3d target_position = gAgent.getRegion()->getPosGlobalFromRegion(LLVector3(x, y, z));
			else target_position = gAgent.getRegion()->getPosGlobalFromRegion(LLVector3(x, y, target_position.mdV[VZ]));

			gAgent.teleportViaLocation(target_position);

			return true;
		}

		systemTip(INVALID_CHAT_COMMAND);

		return true;
	}
	// Region wide chat commands, made for testing purposes and exploding our old sim.
	/*else if (command == "/physdelink")
	{
		S32 currentIndex = 0;
		S32 numberOfObjects = gObjectList.getNumObjects();
		while (currentIndex < numberOfObjects)
		{
			LLViewerObject * objectPointer = gObjectList.getObject(currentIndex++);
			if (objectPointer &&
			   !objectPointer->isAvatar() &&
			   !objectPointer->isAttachment())
			{
				if (objectPointer->permModify() &&
					objectPointer->isRoot() &&
				   !objectPointer->isJointChild())
				{
					LLViewerRegion* regionPointer = objectPointer->getRegion();
					if(!regionPointer) continue;
					
					//objectPointer->setFlags(FLAGS_PHANTOM, FALSE);
					objectPointer->setFlags(FLAGS_USE_PHYSICS, TRUE);

					gMessageSystem->newMessage("ObjectDelink");
					gMessageSystem->nextBlockFast(_PREHASH_AgentData);
					gMessageSystem->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
					gMessageSystem->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
					gMessageSystem->nextBlockFast(_PREHASH_ObjectData);
					gMessageSystem->addU32Fast(_PREHASH_ObjectLocalID, objectPointer->getLocalID());
					gMessageSystem->sendReliable(regionPointer->getHost());

					objectPointer->setFlags(FLAGS_USE_PHYSICS, TRUE);
				}
			}
		}

		return true;
	}
	else if (command == "/physlock")
	{
		S32 currentIndex = 0;
		S32 numberOfObjects = gObjectList.getNumObjects();
		while (currentIndex < numberOfObjects)
		{
			LLViewerObject * objectPointer = gObjectList.getObject(currentIndex++);
			if (objectPointer &&
			   !objectPointer->isAvatar() &&
			   !objectPointer->isAttachment())
			{
				if (objectPointer->permModify() &&
					objectPointer->isRoot() &&
				   !objectPointer->isJointChild())
				{
					objectPointer->setFlags(FLAGS_USE_PHYSICS, FALSE);
				}
			}
		}

		return true;
	}
	else if (command == "/unphantom")
	{
		S32 currentIndex = 0;
		S32 numberOfObjects = gObjectList.getNumObjects();
		while (currentIndex < numberOfObjects)
		{
			LLViewerObject * objectPointer = gObjectList.getObject(currentIndex++);
			if (objectPointer &&
			   !objectPointer->isAvatar() &&
			   !objectPointer->isAttachment())
			{
				if (objectPointer->permModify() &&
					objectPointer->isRoot() &&
				   !objectPointer->isJointChild())
				{
					objectPointer->setFlags(FLAGS_PHANTOM, FALSE);
				}
			}
		}

		return true;
	}*/

	return false;
}

void exoChatCommands::systemTip(std::string data)
{
	if (data.empty()) return;

	LLSD args;
	args["MESSAGE"] = data;
	LLNotificationsUtil::add("SystemMessageTip", args);
}

void exoChatCommands::systemMessage(std::string data)
{
	if (data.empty()) return;

	LLChat chat;

	chat.mText = data;
	chat.mSourceType = CHAT_SOURCE_SYSTEM;

	LLSD args;
	args["type"] = LLNotificationsUI::NT_NEARBYCHAT;

	LLNotificationsUI::LLNotificationManager::instance().onChat(chat, args);
}

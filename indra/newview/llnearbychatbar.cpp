/** 
 * @file llnearbychatbar.cpp
 * @brief LLNearbyChatBar class implementation
 *
 * $LicenseInfo:firstyear=2002&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "message.h"

#include "llappviewer.h"
#include "llfloaterreg.h"
#include "lltrans.h"

#include "llfirstuse.h"
#include "llnearbychatbar.h"
#include "llagent.h"
#include "llgesturemgr.h"
#include "llmultigesture.h"
#include "llkeyboard.h"
#include "llanimationstates.h"
#include "llviewerstats.h"
#include "llcommandhandler.h"
#include "llviewercontrol.h"
#include "llnavigationbar.h"
#include "llwindow.h"
#include "llviewerwindow.h"
#include "llrootview.h"
#include "llviewerchat.h"
#include "llnearbychat.h"
#include "lltranslate.h"

// [RLVa:KB] - Checked: 2010-02-27 (RLVa-1.2.0b)
#include "rlvhandler.h"
// [/RLVa:KB]

#include "llresizehandle.h"

#include "llconsole.h" // <exodus/>
#include "exochatcommands.h" // <exodus/>

S32 LLNearbyChatBar::sLastSpecialChatChannel = 0;

const S32 EXPANDED_HEIGHT = 300;
const S32 COLLAPSED_HEIGHT = 26;
const S32 EXPANDED_MIN_HEIGHT = 150;

// legacy callback glue
//void send_chat_from_viewer(const std::string& utf8_out_text, EChatType type, S32 channel);
// [RLVa:KB] - Checked: 2010-02-27 (RLVa-1.2.0b) | Modified: RLVa-0.2.2a
void send_chat_from_viewer(std::string utf8_out_text, EChatType type, S32 channel);
// [/RLVa:KB]

struct LLChatTypeTrigger {
	std::string name;
	EChatType type;
};

static LLChatTypeTrigger sChatTypeTriggers[] = {
	{ "/whisper"	, CHAT_TYPE_WHISPER},
	{ "/shout"	, CHAT_TYPE_SHOUT}
};

LLNearbyChatBar::LLNearbyChatBar(const LLSD& key)
:	LLFloater(key),
	mChatBox(NULL),
	mSecondChatBox(NULL), // <exodus/>
	mChatHistory(NULL),
	mOutputMonitor(NULL),
	mSecondOutputMonitor(NULL), // <exodus/>
	mSpeakerMgr(NULL),
	mExpandedHeight(COLLAPSED_HEIGHT + EXPANDED_HEIGHT)
{
	mSpeakerMgr = LLLocalSpeakerMgr::getInstance();
}

//virtual
BOOL LLNearbyChatBar::postBuild()
{
	mChatBox = getChild<LLLineEditor>("chat_box");

	mChatBox->setCommitCallback(boost::bind(&LLNearbyChatBar::onChatBoxCommit, this));
	mChatBox->setKeystrokeCallback(&onChatBoxKeystroke, this);
	mChatBox->setFocusLostCallback(boost::bind(&onChatBoxFocusLost, _1, this));
	mChatBox->setFocusReceivedCallback(boost::bind(&LLNearbyChatBar::onChatBoxFocusReceived, this));

	mChatBox->setIgnoreArrowKeys( FALSE ); 
	mChatBox->setCommitOnFocusLost( FALSE );
	mChatBox->setRevertOnEsc( FALSE );
	mChatBox->setIgnoreTab(TRUE);
	mChatBox->setPassDelete(TRUE);
	mChatBox->setReplaceNewlinesWithSpaces(FALSE);
	mChatBox->setEnableLineHistory(TRUE);
	mChatBox->setFont(LLViewerChat::getChatFont());
	
	// <exodus>
	mChatHistory = getChild<LLNearbyChat>("nearby_chat");

	LLUICtrl* show_btn = getChild<LLUICtrl>("show_nearby_chat");
	show_btn->setCommitCallback(boost::bind(&LLNearbyChatBar::onToggleNearbyChatPanel, this));

	gSavedSettings.declareBOOL("nearbychat_history_visibility", mChatHistory->getVisible(), "Visibility state of nearby chat history", TRUE);

	mSecondChatBox = mChatHistory->getChild<LLLineEditor>("chat_box");
	mSecondChatBox->setCommitCallback(boost::bind(&LLNearbyChatBar::onChatBoxCommit, this));
	mSecondChatBox->setKeystrokeCallback(&onChatBoxKeystroke, this);
	mSecondChatBox->setFocusLostCallback(boost::bind(&onChatBoxFocusLost, _1, this));
	mSecondChatBox->setFocusReceivedCallback(boost::bind(&LLNearbyChatBar::onChatBoxFocusReceived, this));
	mSecondChatBox->setIgnoreArrowKeys( FALSE ); 
	mSecondChatBox->setCommitOnFocusLost( FALSE );
	mSecondChatBox->setRevertOnEsc( FALSE );
	mSecondChatBox->setIgnoreTab(TRUE);
	mSecondChatBox->setPassDelete(TRUE);
	mSecondChatBox->setReplaceNewlinesWithSpaces(FALSE);
	mSecondChatBox->setEnableLineHistory(TRUE);
	mSecondChatBox->setFont(LLViewerChat::getChatFont());
	
	LLUICtrl* second_show_btn = mChatHistory->getChild<LLUICtrl>("show_nearby_chat");
	second_show_btn->setCommitCallback(boost::bind(&LLNearbyChatBar::onToggleNearbyChatPanel, this));
	
	mOutputMonitor = getChild<LLOutputMonitorCtrl>("chat_zone_indicator");
	mOutputMonitor->setVisible(FALSE);

	mSecondOutputMonitor = mChatHistory->getChild<LLOutputMonitorCtrl>("chat_zone_indicator");
	mSecondOutputMonitor->setVisible(FALSE);

	setVisibleCallback(boost::bind(&LLNearbyChatBar::updateConsoleVisibility, this));
	// <exodus>

	// Register for font change notifications
	LLViewerChat::setFontChangedCallback(boost::bind(&LLNearbyChatBar::onChatFontChange, this, _1));

	enableResizeCtrls(true, true, false);
	
	if(gSavedSettings.getBOOL("nearbychat_history_visibility")) onToggleNearbyChatPanel();

	return TRUE;
}

// virtual
void LLNearbyChatBar::onOpen(const LLSD& key)
{
	enableTranslationCheckbox(LLTranslate::isTranslationConfigured());
}

// <exodus>
void LLNearbyChatBar::updateConsoleVisibility()
{
	if(gDisconnected) return;

	gConsole->setVisible(!isInVisibleChain() || !mChatHistory->getVisible());
}
// </exodus>

bool LLNearbyChatBar::applyRectControl()
{
	bool rect_controlled = LLFloater::applyRectControl();

	if (!mChatHistory->getVisible()) // <exodus/>
	{
		setTitle(""); // <exodus/>
		setBackgroundImage(LLUI::getUIImage("Window_NoTitle_Foreground")); // <exodus/>
		setTransparentImage(LLUI::getUIImage("Window_NoTitle_Background")); // <exodus/>
		gConsole->setVisible(TRUE); // <exodus/>
		reshape(getRect().getWidth(), getMinHeight());
		enableResizeCtrls(true, true, false);
	}
	else
	{
		setTitle("NEARBY CHAT"); // <exodus/>
		setBackgroundImage(LLUI::getUIImage("Window_Foreground")); // <exodus/>
		setTransparentImage(LLUI::getUIImage("Window_Background")); // <exodus/>
		gConsole->setVisible(FALSE); // <exodus/>
		enableResizeCtrls(true);
		setResizeLimits(getMinWidth(), EXPANDED_MIN_HEIGHT);
	}
	
	return rect_controlled;
}

void LLNearbyChatBar::saveChatHistoryVisibility()
{
	// save visibility state of nearby chat history panel if
	// visibility of nearby chat floater is saved, i.e. save_visisbility="true"
	// (if save_visisbility="true", mVisibilityControl == "floater_vis_chat_bar")
	if (mVisibilityControl.size() > 1)
	{
		// save visibility of nearby chat history
		gSavedSettings.setBOOL("nearbychat_history_visibility", mChatHistory->getVisible());
	}
}

void LLNearbyChatBar::onChatFontChange(LLFontGL* fontp)
{
	// Update things with the new font whohoo
	if (mChatBox) mChatBox->setFont(fontp);
	if (mSecondChatBox) mSecondChatBox->setFont(fontp); // <exodus/>
}

//static
LLNearbyChatBar* LLNearbyChatBar::getInstance()
{
	return LLFloaterReg::getTypedInstance<LLNearbyChatBar>("chat_bar");
}

void LLNearbyChatBar::showHistory()
{
	openFloater();

	if (!mChatHistory->getVisible()) // <exodus/>
	{
		onToggleNearbyChatPanel();
	}
}

void LLNearbyChatBar::enableTranslationCheckbox(BOOL enable)
{
	getChild<LLUICtrl>("translate_chat_checkbox")->setEnabled(enable);
}

void LLNearbyChatBar::draw()
{
	displaySpeakingIndicator();
	LLFloater::draw();
}

std::string LLNearbyChatBar::getCurrentChat()
{
	if (mChatHistory->getVisible()) return mSecondChatBox ? mSecondChatBox->getText() : LLStringUtil::null;
	else return mChatBox ? mChatBox->getText() : LLStringUtil::null;
}

// virtual
BOOL LLNearbyChatBar::handleKeyHere( KEY key, MASK mask )
{
	BOOL handled = FALSE;
	
	// <exodus>
	if( KEY_RETURN == key )
	{
		if( mask == MASK_CONTROL )
		{
			// shout
			sendChat(CHAT_TYPE_SHOUT);
			handled = TRUE;
		}
		else if( mask == MASK_SHIFT )
		{
			// whisper
			sendChat(CHAT_TYPE_WHISPER);
			handled = TRUE;
		}
	}
	// </exodus>

	return handled;
}

BOOL LLNearbyChatBar::matchChatTypeTrigger(const std::string& in_str, std::string* out_str)
{
	U32 in_len = in_str.length();
	S32 cnt = sizeof(sChatTypeTriggers) / sizeof(*sChatTypeTriggers);
	
	for (S32 n = 0; n < cnt; n++)
	{
		if (in_len > sChatTypeTriggers[n].name.length())
			continue;

		std::string trigger_trunc = sChatTypeTriggers[n].name;
		LLStringUtil::truncate(trigger_trunc, in_len);

		if (!LLStringUtil::compareInsensitive(in_str, trigger_trunc))
		{
			*out_str = sChatTypeTriggers[n].name;
			return TRUE;
		}
	}

	return FALSE;
}

void LLNearbyChatBar::onChatBoxKeystroke(LLLineEditor* caller, void* userdata)
{
	LLFirstUse::otherAvatarChatFirst(false);

	LLNearbyChatBar* self = (LLNearbyChatBar *)userdata;

	LLWString raw_text; // <exodus/>

	if (self->mChatHistory->getVisible()) raw_text = self->mSecondChatBox->getWText(); // <exodus/>
	else raw_text = self->mChatBox->getWText(); // <exodus/>

	// Can't trim the end, because that will cause autocompletion
	// to eat trailing spaces that might be part of a gesture.
	LLWStringUtil::trimHead(raw_text);

	S32 length = raw_text.length();

// <exodus>
	static LLCachedControl<bool> sSilentOOC(gSavedSettings, "ExodusOOCDontAnimate", FALSE);
	static LLCachedControl<bool> sAllowMU(gSavedSettings, "ExodusAllowMU", FALSE);
	char start = raw_text[0];

	if (length > 0
		&& !(start == '/' || ((start == '(' || start == '[') && sSilentOOC) || (start == ':' && sAllowMU))
		&& !gRlvHandler.hasBehaviour(RLV_BHVR_REDIRCHAT))
// </exodus>
	{
		gAgent.startTyping();
	}
	else
	{
		gAgent.stopTyping();
	}

	/* Doesn't work -- can't tell the difference between a backspace
	   that killed the selection vs. backspace at the end of line.
	if (length > 1 
		&& text[0] == '/'
		&& key == KEY_BACKSPACE)
	{
		// the selection will already be deleted, but we need to trim
		// off the character before
		std::string new_text = raw_text.substr(0, length-1);
		self->mInputEditor->setText( new_text );
		self->mInputEditor->setCursorToEnd();
		length = length - 1;
	}
	*/

	KEY key = gKeyboard->currentKey();

	// Ignore "special" keys, like backspace, arrows, etc.
	if (length > 1 
		&& raw_text[0] == '/'
		&& key < KEY_SPECIAL)
	{
		// we're starting a gesture, attempt to autocomplete

		std::string utf8_trigger = wstring_to_utf8str(raw_text);
		std::string utf8_out_str(utf8_trigger);

		if (LLGestureMgr::instance().matchPrefix(utf8_trigger, &utf8_out_str))
		{
			std::string rest_of_match = utf8_out_str.substr(utf8_trigger.size());
			self->mChatBox->setText(utf8_trigger + rest_of_match); // keep original capitalization for user-entered part
			self->mSecondChatBox->setText(utf8_trigger + rest_of_match); // <exodus/>
			S32 outlength = self->mChatBox->getLength(); // in characters

			// Select to end of line, starting from the character
			// after the last one the user typed.
			self->mChatBox->setSelection(length, outlength);
			self->mSecondChatBox->setSelection(length, outlength); // <exodus/>
		}
		else if (matchChatTypeTrigger(utf8_trigger, &utf8_out_str))
		{
			std::string rest_of_match = utf8_out_str.substr(utf8_trigger.size());
			self->mChatBox->setText(utf8_trigger + rest_of_match + " "); // keep original capitalization for user-entered part
			self->mSecondChatBox->setText(utf8_trigger + rest_of_match + " "); // <exodus/>
			self->mChatBox->setCursorToEnd();
			self->mSecondChatBox->setCursorToEnd(); // <exodus/>
		}

		//llinfos << "GESTUREDEBUG " << trigger 
		//	<< " len " << length
		//	<< " outlen " << out_str.getLength()
		//	<< llendl;
	}
}

// static
void LLNearbyChatBar::onChatBoxFocusLost(LLFocusableElement* caller, void* userdata)
{
	// stop typing animation
	gAgent.stopTyping();
}

void LLNearbyChatBar::onChatBoxFocusReceived()
{
	mChatBox->setEnabled(!gDisconnected);
	mSecondChatBox->setEnabled(!gDisconnected); // <exodus/>
}

EChatType LLNearbyChatBar::processChatTypeTriggers(EChatType type, std::string &str)
{
	U32 length = str.length();
	S32 cnt = sizeof(sChatTypeTriggers) / sizeof(*sChatTypeTriggers);
	
	for (S32 n = 0; n < cnt; n++)
	{
		if (length >= sChatTypeTriggers[n].name.length())
		{
			std::string trigger = str.substr(0, sChatTypeTriggers[n].name.length());

			if (!LLStringUtil::compareInsensitive(trigger, sChatTypeTriggers[n].name))
			{
				U32 trigger_length = sChatTypeTriggers[n].name.length();

				// It's to remove space after trigger name
				if (length > trigger_length && str[trigger_length] == ' ')
					trigger_length++;

				str = str.substr(trigger_length, length);

				if (CHAT_TYPE_NORMAL == type)
					return sChatTypeTriggers[n].type;
				else
					break;
			}
		}
	}

	return type;
}

void LLNearbyChatBar::sendChat( EChatType type )
{
	if (mChatBox && mSecondChatBox)
	{
		// <exodus>
		LLWString text;

		if (mChatHistory->getVisible()) // <exodus/>
		{
			text = mSecondChatBox->getConvertedText();
			mChatBox->setText(mSecondChatBox->getText());
		}
		else
		{
			text = mChatBox->getConvertedText();
			mSecondChatBox->setText(mChatBox->getText());
		}
		// </exodus>

		if (!text.empty())
		{
			// store sent line in history, duplicates will get filtered
			mChatBox->updateHistory();
			mSecondChatBox->updateHistory(); // <exodus/>
			// Check if this is destined for another channel
			S32 channel = 0;
			stripChannelNumber(text, &channel);
			
			std::string utf8text = wstring_to_utf8str(text);
			// Try to trigger a gesture, if not chat to a script.
			std::string utf8_revised_text;
			if (0 == channel)
			{
// <exodus>
//-TT Patch MU_OOC from Satomi Ahn
				if (gSavedSettings.getBOOL("ExodusOOCAutoClose"))
				{
					// Try to find any unclosed OOC chat (i.e. an opening
					// double parenthesis without a matching closing double
					// parenthesis.
					if (utf8text.find("(( ") != -1 && utf8text.find("))") == -1)
					{
						// add the missing closing double parenthesis.
						utf8text += " ))";
					}
					else if (utf8text.find("((") != -1 && utf8text.find("))") == -1)
					{
						if (utf8text.at(utf8text.length() - 1) == ')')
						{
							// cosmetic: add a space first to avoid a closing triple parenthesis
							utf8text += " ";
						}
						// add the missing closing double parenthesis.
						utf8text += "))";
					}
					else if (utf8text.find("[[ ") != -1 && utf8text.find("]]") == -1)
					{
						// add the missing closing double parenthesis.
						utf8text += " ]]";
					}
					else if (utf8text.find("[[") != -1 && utf8text.find("]]") == -1)
					{
						if (utf8text.at(utf8text.length() - 1) == ']')
						{
							// cosmetic: add a space first to avoid a closing triple parenthesis
							utf8text += " ";
						}
						// add the missing closing double parenthesis.
						utf8text += "]]";
					}
				}

				// Convert MU*s style poses into IRC emotes here.
				if (gSavedSettings.getBOOL("ExodusAllowMU") && utf8text.find(":") == 0 && utf8text.length() > 3)
				{
					if (utf8text.find(":'") == 0)
					{
						utf8text.replace(0, 1, "/me");
	 				}
 					else if (isalpha(utf8text.at(1)))	// Do not prevent smileys and such.
					{
						utf8text.replace(0, 1, "/me ");
					}
				}
//-TT Patch MU_OOC from Satomi Ahn

				// discard returned "found" boolean
				LLGestureMgr::instance().triggerAndReviseString(utf8text, &utf8_revised_text);
			}
			else
			{
				utf8_revised_text = utf8text;
			}

			utf8_revised_text = utf8str_trim(utf8_revised_text);

			type = processChatTypeTriggers(type, utf8_revised_text);

			if (!utf8_revised_text.empty())
			{
				// <exodus>
				if (!exoChatCommands::phraseChat(utf8_revised_text))
				{
					// Chat with animation
					sendChatFromViewer(utf8_revised_text, type, TRUE);
				}
				// </exodus>
			}
		}

		mChatBox->setText(LLStringExplicit(""));
		mSecondChatBox->setText(LLStringExplicit("")); // <exodus/>
	}

	gAgent.stopTyping();

	// If the user wants to stop chatting on hitting return, lose focus
	// and go out of chat mode.
	if (gSavedSettings.getBOOL("CloseChatOnReturn"))
	{
		stopChat();
	}
}

// <exodus>
void LLNearbyChatBar::onToggleNearbyChatPanel()
{
	if (mChatHistory->getVisible()) // <exodus/>
	{
		if (!isMinimized()) mExpandedHeight = getRect().getHeight();
		
		setTitle(""); // <exodus/>
		setBackgroundImage(LLUI::getUIImage("Window_NoTitle_Foreground")); // <exodus/>
		setTransparentImage(LLUI::getUIImage("Window_NoTitle_Background")); // <exodus/>
		setResizeLimits(getMinWidth(), COLLAPSED_HEIGHT);
		getChildView("move_handle")->setVisible(TRUE);
		getChild<LLPanel>("chat_panel")->setVisible(TRUE);
		mChatBox->setText(mSecondChatBox->getText());
		mChatHistory->setVisible(FALSE); // <exodus/>
		reshape(getRect().getWidth(), COLLAPSED_HEIGHT);
		enableResizeCtrls(true, true, false);
		gConsole->setVisible(TRUE); // <exodus/>

		storeRectControl();
	}
	else
	{
		setTitle("NEARBY CHAT"); // <exodus/>
		setBackgroundImage(LLUI::getUIImage("Window_Foreground")); // <exodus/>
		setTransparentImage(LLUI::getUIImage("Window_Background")); // <exodus/>
		getChildView("move_handle")->setVisible(FALSE);
		getChild<LLPanel>("chat_panel")->setVisible(FALSE);
		mSecondChatBox->setText(mChatBox->getText());
		mChatHistory->setVisible(TRUE); // <exodus/>
		setResizeLimits(getMinWidth(), EXPANDED_MIN_HEIGHT);
		reshape(getRect().getWidth(), mExpandedHeight);
		enableResizeCtrls(true);
		gConsole->setVisible(FALSE); // <exodus/>

		storeRectControl();
	}

	saveChatHistoryVisibility();
}
// </exodus>

// <exodus>
/*void LLNearbyChatBar::setMinimized(BOOL b)
{
	// when unminimizing with nearby chat visible, go ahead and kill off screen chats
	if (!b && mChatHistory->getVisible()) // <exodus/>
	{
		mChatHistory->removeScreenChat(); // <exodus/>
	}

	LLFloater::setMinimized(b);
}*/
// </exodus>

void LLNearbyChatBar::onChatBoxCommit()
{
	// <exodus>
	if (mChatHistory->getVisible()) // <exodus/>
	{
		if (mSecondChatBox->getText().length() > 0)
		{
			sendChat(CHAT_TYPE_NORMAL);
		}
	}
	else if (mChatBox->getText().length() > 0)
	{
		sendChat(CHAT_TYPE_NORMAL);
	}
	// </exodus>

	gAgent.stopTyping();
}

void LLNearbyChatBar::displaySpeakingIndicator()
{
	LLSpeakerMgr::speaker_list_t speaker_list;
	LLUUID id;

	id.setNull();
	mSpeakerMgr->update(TRUE);
	mSpeakerMgr->getSpeakerList(&speaker_list, FALSE);

	for (LLSpeakerMgr::speaker_list_t::iterator i = speaker_list.begin(); i != speaker_list.end(); ++i)
	{
		LLPointer<LLSpeaker> s = *i;
		if (s->mSpeechVolume > 0 || s->mStatus == LLSpeaker::STATUS_SPEAKING)
		{
			id = s->mID;
			break;
		}
	}

	if (!id.isNull())
	{
		mOutputMonitor->setVisible(TRUE);
		mSecondOutputMonitor->setVisible(TRUE); // <exodus/>
		mOutputMonitor->setSpeakerId(id);
		mSecondOutputMonitor->setSpeakerId(id); // <exodus/>
	}
	else
	{
		mOutputMonitor->setVisible(FALSE);
		mSecondOutputMonitor->setVisible(FALSE); // <exodus/>
	}
}

void LLNearbyChatBar::sendChatFromViewer(const std::string &utf8text, EChatType type, BOOL animate)
{
	sendChatFromViewer(utf8str_to_wstring(utf8text), type, animate);
}

void LLNearbyChatBar::sendChatFromViewer(const LLWString &wtext, EChatType type, BOOL animate)
{
	// Look for "/20 foo" channel chats.
	S32 channel = 0;
	LLWString out_text = stripChannelNumber(wtext, &channel);
	std::string utf8_out_text = wstring_to_utf8str(out_text);
	std::string utf8_text = wstring_to_utf8str(wtext);

	utf8_text = utf8str_trim(utf8_text);
	if (!utf8_text.empty())
	{
		utf8_text = utf8str_truncate(utf8_text, MAX_STRING - 1);
	}

// [RLVa:KB] - Checked: 2010-03-27 (RLVa-1.2.0b) | Modified: RLVa-1.2.0b
	if ( (0 == channel) && (rlv_handler_t::isEnabled()) )
	{
		// Adjust the (public) chat "volume" on chat and gestures (also takes care of playing the proper animation)
		if ( ((CHAT_TYPE_SHOUT == type) || (CHAT_TYPE_NORMAL == type)) && (gRlvHandler.hasBehaviour(RLV_BHVR_CHATNORMAL)) )
			type = CHAT_TYPE_WHISPER;
		else if ( (CHAT_TYPE_SHOUT == type) && (gRlvHandler.hasBehaviour(RLV_BHVR_CHATSHOUT)) )
			type = CHAT_TYPE_NORMAL;
		else if ( (CHAT_TYPE_WHISPER == type) && (gRlvHandler.hasBehaviour(RLV_BHVR_CHATWHISPER)) )
			type = CHAT_TYPE_NORMAL;

		animate &= !gRlvHandler.hasBehaviour( (!RlvUtil::isEmote(utf8_text)) ? RLV_BHVR_REDIRCHAT : RLV_BHVR_REDIREMOTE );
	}
// [/RLVa:KB]

	// Don't animate for chats people can't hear (chat to scripts)
	if (animate && (channel == 0))
	{
		if (type == CHAT_TYPE_WHISPER)
		{
			lldebugs << "You whisper " << utf8_text << llendl;
			gAgent.sendAnimationRequest(ANIM_AGENT_WHISPER, ANIM_REQUEST_START);
		}
		else if (type == CHAT_TYPE_NORMAL)
		{
			lldebugs << "You say " << utf8_text << llendl;
			gAgent.sendAnimationRequest(ANIM_AGENT_TALK, ANIM_REQUEST_START);
		}
		else if (type == CHAT_TYPE_SHOUT)
		{
			lldebugs << "You shout " << utf8_text << llendl;
			gAgent.sendAnimationRequest(ANIM_AGENT_SHOUT, ANIM_REQUEST_START);
		}
		else
		{
			llinfos << "send_chat_from_viewer() - invalid volume" << llendl;
			return;
		}
	}
	else
	{
		if (type != CHAT_TYPE_START && type != CHAT_TYPE_STOP)
		{
			lldebugs << "Channel chat: " << utf8_text << llendl;
		}
	}

	send_chat_from_viewer(utf8_out_text, type, channel);
}

// static 
void LLNearbyChatBar::startChat(const char* line)
{
	LLNearbyChatBar* cb = LLNearbyChatBar::getInstance();

	if (!cb )
		return;

	cb->setVisible(TRUE);
	cb->setFocus(TRUE);

	if (cb->mChatHistory->getVisible()) cb->mSecondChatBox->setFocus(TRUE); // <exodus/>
	else cb->mChatBox->setFocus(TRUE); // <exodus/>

	if (line)
	{
		std::string line_string(line);
		cb->mChatBox->setText(line_string);
		cb->mSecondChatBox->setText(line_string); // <exodus/>
	}

	cb->mChatBox->setCursorToEnd();
	cb->mSecondChatBox->setCursorToEnd(); // <exodus/>
}

// Exit "chat mode" and do the appropriate focus changes
// static
void LLNearbyChatBar::stopChat()
{
	LLNearbyChatBar* cb = LLNearbyChatBar::getInstance();

	if (!cb)
		return;

	if (cb->mChatHistory->getVisible()) cb->mSecondChatBox->setFocus(FALSE); // <exodus/>
	else cb->mChatBox->setFocus(FALSE); // <exodus/>

 	// stop typing animation
 	gAgent.stopTyping();
}

// If input of the form "/20foo" or "/20 foo", returns "foo" and channel 20.
// Otherwise returns input and channel 0.
LLWString LLNearbyChatBar::stripChannelNumber(const LLWString &mesg, S32* channel)
{
	if (mesg[0] == '/'
		&& mesg[1] == '/')
	{
		// This is a "repeat channel send"
		*channel = sLastSpecialChatChannel;
		return mesg.substr(2, mesg.length() - 2);
	}
	else if (mesg[0] == '/'
			 && mesg[1]
			 && LLStringOps::isDigit(mesg[1]))
	{
		// This a special "/20" speak on a channel
		S32 pos = 0;

		// Copy the channel number into a string
		LLWString channel_string;
		llwchar c;
		do
		{
			c = mesg[pos+1];
			channel_string.push_back(c);
			pos++;
		}
		while(c && pos < 64 && LLStringOps::isDigit(c));
		
		// Move the pointer forward to the first non-whitespace char
		// Check isspace before looping, so we can handle "/33foo"
		// as well as "/33 foo"
		while(c && iswspace(c))
		{
			c = mesg[pos+1];
			pos++;
		}
		
		sLastSpecialChatChannel = strtol(wstring_to_utf8str(channel_string).c_str(), NULL, 10);
		*channel = sLastSpecialChatChannel;
		return mesg.substr(pos, mesg.length() - pos);
	}
	else
	{
		// This is normal chat.
		*channel = 0;
		return mesg;
	}
}

//void send_chat_from_viewer(const std::string& utf8_out_text, EChatType type, S32 channel)
// [RLVa:KB] - Checked: 2010-02-27 (RLVa-1.2.0b) | Modified: RLVa-0.2.2a
void send_chat_from_viewer(std::string utf8_out_text, EChatType type, S32 channel)
// [/RLVa:KB]
{
// [RLVa:KB] - Checked: 2010-02-27 (RLVa-1.2.0b) | Modified: RLVa-1.2.0a
	// Only process chat messages (ie not CHAT_TYPE_START, CHAT_TYPE_STOP, etc)
	if ( (rlv_handler_t::isEnabled()) && ( (CHAT_TYPE_WHISPER == type) || (CHAT_TYPE_NORMAL == type) || (CHAT_TYPE_SHOUT == type) ) )
	{
		if (0 == channel)
		{
			// (We already did this before, but LLChatHandler::handle() calls this directly)
			if ( ((CHAT_TYPE_SHOUT == type) || (CHAT_TYPE_NORMAL == type)) && (gRlvHandler.hasBehaviour(RLV_BHVR_CHATNORMAL)) )
				type = CHAT_TYPE_WHISPER;
			else if ( (CHAT_TYPE_SHOUT == type) && (gRlvHandler.hasBehaviour(RLV_BHVR_CHATSHOUT)) )
				type = CHAT_TYPE_NORMAL;
			else if ( (CHAT_TYPE_WHISPER == type) && (gRlvHandler.hasBehaviour(RLV_BHVR_CHATWHISPER)) )
				type = CHAT_TYPE_NORMAL;

			// Redirect chat if needed
			if ( ( (gRlvHandler.hasBehaviour(RLV_BHVR_REDIRCHAT) || (gRlvHandler.hasBehaviour(RLV_BHVR_REDIREMOTE)) ) && 
				 (gRlvHandler.redirectChatOrEmote(utf8_out_text)) ) )
			{
				return;
			}

			// Filter public chat if sendchat restricted
			if (gRlvHandler.hasBehaviour(RLV_BHVR_SENDCHAT))
				gRlvHandler.filterChat(utf8_out_text, true);
		}
		else
		{
			// Don't allow chat on a non-public channel if sendchannel restricted (unless the channel is an exception)
			if ( (gRlvHandler.hasBehaviour(RLV_BHVR_SENDCHANNEL)) && (!gRlvHandler.isException(RLV_BHVR_SENDCHANNEL, channel)) )
				return;

			// Don't allow chat on debug channel if @sendchat, @redirchat or @rediremote restricted (shows as public chat on viewers)
			if (CHAT_CHANNEL_DEBUG == channel)
			{
				bool fIsEmote = RlvUtil::isEmote(utf8_out_text);
				if ( (gRlvHandler.hasBehaviour(RLV_BHVR_SENDCHAT)) || 
					 ((!fIsEmote) && (gRlvHandler.hasBehaviour(RLV_BHVR_REDIRCHAT))) || 
					 ((fIsEmote) && (gRlvHandler.hasBehaviour(RLV_BHVR_REDIREMOTE))) )
				{
					return;
				}
			}
		}
	}
// [/RLVa:KB]

	LLMessageSystem* msg = gMessageSystem;
	msg->newMessageFast(_PREHASH_ChatFromViewer);
	msg->nextBlockFast(_PREHASH_AgentData);
	msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
	msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
	msg->nextBlockFast(_PREHASH_ChatData);
	msg->addStringFast(_PREHASH_Message, utf8_out_text);
	msg->addU8Fast(_PREHASH_Type, type);
	msg->addS32("Channel", channel);

	gAgent.sendReliableMessage();

	LLViewerStats::getInstance()->incStat(LLViewerStats::ST_CHAT_COUNT);
}

class LLChatCommandHandler : public LLCommandHandler
{
public:
	// not allowed from outside the app
	LLChatCommandHandler() : LLCommandHandler("chat", UNTRUSTED_BLOCK) { }

    // Your code here
	bool handle(const LLSD& tokens, const LLSD& query_map,
				LLMediaCtrl* web)
	{
		bool retval = false;
		// Need at least 2 tokens to have a valid message.
		if (tokens.size() < 2)
		{
			retval = false;
		}
		else
		{
		S32 channel = tokens[0].asInteger();
			// VWR-19499 Restrict function to chat channels greater than 0.
			if ((channel > 0) && (channel < CHAT_CHANNEL_DEBUG))
			{
				retval = true;
		// Send unescaped message, see EXT-6353.
		std::string unescaped_mesg (LLURI::unescape(tokens[1].asString()));
		send_chat_from_viewer(unescaped_mesg, CHAT_TYPE_NORMAL, channel);
			}
			else
			{
				retval = false;
				// Tell us this is an unsupported SLurl.
			}
		}
		return retval;
	}
};

// Creating the object registers with the dispatcher.
LLChatCommandHandler gChatHandler;



/** 
 * @file exobridge.h
 * @brief exoBridge class definitions
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_API
#define EXO_API

// Command prefixes:
#define EAPI_CMD_PREFIX		'#' // For legacy Genesis commands to keep old Ark content working.

#include "lleventtimer.h"
#include "llsingleton.h"

// Other non-included classes:
class LLViewerObject;

// Main class definition:


class eAPI
:	public LLEventTimer,
	public LLSingleton<eAPI>
{
	public:
		
		static LLUUID ghost_key;
		static BOOL ignoreAgents;

		eAPI();
		~eAPI();

		void startTimer();//F32 timeout);
		void stopTimer();

		virtual BOOL tick();
		static void idleCall();
		static BOOL isEnabled();
		static void dataStream(std::string data);
		static BOOL parseSounds(LLUUID sound_id, LLUUID source_id, LLUUID source_owner_id);
		static BOOL phraseChat(std::string input, LLViewerObject* source_obj, LLUUID source_id);
		static void ghostObject(LLVector3 pos, LLQuaternion rot);
	private:
		static void eAPIDebug(BOOL debug, std::string command, std::string debug_message);
		static void sendResponse(LLUUID target, S32 channel, std::string command, std::string response);
		static void eAPISit(LLUUID target);
		static void eAPITouch(LLViewerObject* touchObject);
		static void mouseRaycast(S32 channel, F32 depth, BOOL ignoreagents, std::string label, LLUUID source_id);
		static void performRaycast(LLVector3 target, LLVector3 source, BOOL ignoreagents, LLUUID source_id,S32 channel, std::string label);
		static void ghostStop();
};

#endif // EXO_API

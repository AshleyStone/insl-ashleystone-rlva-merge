/** 
 * @file exofloateradvancedradar.cpp
 * @brief The advanced radar window for managing simulators.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exofloateradvancedradar.h"

#include "llfloaterreg.h"

// exodus_floater_advanced_radar.xml

exoFloaterAdvancedRadar::exoFloaterAdvancedRadar(const LLSD& key) :
	LLFloater(key)
{
}

BOOL exoFloaterAdvancedRadar::postBuild()
{
	return TRUE;
}
